import unittest
import error

class TestError(unittest.TestCase):
    def test_error_create(self):
        try:
            raise error.Error(error.INTERNAL_ERROR, "internal error")
        except error.Error as e:
            self.assertEqual(e.get_error_code(), error.INTERNAL_ERROR)
            self.assertEqual(e.get_error_message(), "internal error")