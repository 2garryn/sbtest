import utils
import error


class Balance:
    """Balance class is used to represent user account in system.
    Charging money divided into two stages:
        1. First stage is charging money from "real balance" of user
        and adding it to "balance on hold".
        2. Second stage depends on result of payment.
            a) In case of success payment money is charged from "balance on hold".
            b) When payment fails money is returned to "real balance" from "balance on hold"
    Money can be added directly to "real balance" in case of simple income transaction.
    real balance - money that can be spent by user.
    balance on hold - money that waiting cancelling/confirmation.
    """
    def __init__(self, user, balance_real, balance_on_hold=0):
        """Create user account and set start value for balances.

        Parameters
        ----------
        user : str
            Name of user
        balance_real : str, int
            Start value for "real balance"
        balance_on_hold : str, int
            Start value for "balance on hold"
        """
        self.__user = user
        self.__balance_real = utils.to_decimal(balance_real, zeroAllowed=True)
        self.__balance_on_hold = utils.to_decimal(balance_on_hold, zeroAllowed=True)

    def get_user(self):
        """Get owner of balance."""
        return self.__user

    def get_balance_real(self):
        """Get "real balance"."""
        return self.__balance_real

    def get_balance_on_hold(self):
        """Get "balance on hold"."""
        return self.__balance_on_hold

    def charge(self, amount):
        """Charge money from user balance.
        If money was charged it returns True. Otherwise, if user doesn't have enough money, returns False.

        Parameters
        ----------
        amount : str, int
        """
        amount = utils.to_decimal(amount)
        new_real = self.__balance_real - amount
        if new_real.is_signed():
            return False
        else:
            self.__balance_real = new_real
            self.__balance_on_hold = self.__balance_on_hold + amount
            return True

    def charge_commit(self, amount):
        """Confirm charge operation. Money are written-off from "balance on hold"

        Parameters
        ----------
        amount : str, int
        """
        amount = utils.to_decimal(amount)
        new_on_hold = self.__balance_on_hold - amount
        if new_on_hold.is_signed():
            raise error.Error(error.INVALID_AMOUNT, "Can't commit more than on hold")
        self.__balance_on_hold = new_on_hold

    def charge_rollback(self, amount):
        """Cancel charge operation. Money are returned to "real balance"

        Parameters
        ----------
        amount : str, int
        """
        amount = utils.to_decimal(amount)
        new_on_hold = self.__balance_on_hold - amount
        if new_on_hold.is_signed():
            raise error.Error(error.INVALID_AMOUNT, "Can't rollback more than on hold")
        self.__balance_real = self.__balance_real + amount
        self.__balance_on_hold = new_on_hold

    def add(self, amount):
        """Add money to "real balance"

        Parameters
        ----------
        amount : str, int
        """
        amount = utils.to_decimal(amount)
        self.__balance_real = self.__balance_real + amount

    def __str__(self):
        return "User: {0} Real_balance: {1} On_Hold: {2}" . \
            format(self.__user, self.__balance_real, self.__balance_on_hold)
