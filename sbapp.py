"""
Main module that implements test application functionality.
Must be inited before usage.
User should be created before receive/send money.
"""
import decimal
import account_store
import rules
import balance
import transaction
import tx_processor
import error
import log
import utils
from external_service import ExternalService


def init(store, rules_file):
    """Init payment system.
    Parameters:
    ----------
    store : account_store.AccountStore
        Implementation of AccountStore abstract class.
        For this task I used account_file_store.
    rules_file : str
    Returns None for success. Otherwise error.Error will be raised.
    """
    log.l().info("Sbtest starting...")
    account_store.set_store(store)
    rules.load_rules_config(rules_file)
    log.l().info("Sbtest started sucessfully")


def create_user(user, start_amount=0):
    """Create new user in payment system.
    Parameters
    ---------
    user : str
    start_amount : int, str

    if account already exist raises error.Error with code ACCOUNT_ALREADY_EXIST
    """
    dec_amount = utils.to_decimal(start_amount, True)
    log.l().info("Create user {0} with start amount {1}".format(user, start_amount))
    b = balance.Balance(user, dec_amount, decimal.Decimal(0))
    store = account_store.get_store()
    store.create_account(b)
    log.l().info("User {0} created".format(user))


def get_balance(user):
    """Get balance of user
    Returns balance.Balance
    If user not exists raises error.Error with code ACCOUNT_NOT_FOUND
    """
    log.l().debug("Get balance for user {0}".format(user))
    return account_store.get_store().get_balance(user)


def transactions(user, reversed=True):
    """Get list of transactions for passed user
    Parameters
    -----------
    user : str
    reversed : True, False
    Returns list of transactions that may contain transaction.OutcomeTransaction amd transaction.IncomeTransaction.
    By default most recent transaction will be on top of list.
    """
    log.l().debug("List transactions for {0}".format(user))
    txs = account_store.get_store().list_transactions(user)
    txs.sort(key=lambda tx: tx.get_ts(), reverse=reversed)
    return txs


def send_money_to_user(src_user, dst_user, amount):
    """Get list of transactions for passed user
    Send 'amount' from src_user to dst_user.
    Can raise error.Error with codes:
        SEND_ITSELF
        ACCOUNT_NOT_FOUND
        NOT_ENOUGH_MONEY
        RULE_VIOLATION
        INTERNAL_ERROR
    If error happens on destinaion user side outcome transaction will be automatically reverted and removed
    """
    if src_user == dst_user:
        raise error.Error(error.SEND_ITSELF, "can't send money itself")
    dec_amount = utils.to_decimal(amount)
    src_tx_id = transaction.generate_id()
    dst_tx_id = transaction.generate_id()
    es = ExternalService(dst_tx_id, "user2user", dst_user)

    outcome_tx = tx_processor.create_outcome_tx(src_tx_id, src_user, dec_amount, es)
    income_tx = tx_processor.create_income_tx(dst_tx_id, dst_user, dec_amount)
    __log_info("Start", outcome_tx, income_tx)
    tx_processor.outcome_process(outcome_tx)
    try:
        tx_processor.income_complete(income_tx)
        tx_processor.outcome_complete(outcome_tx)
        __log_info("Complete", outcome_tx, income_tx)
    except error.Error as e:
        __log_error(outcome_tx, income_tx, e)
        tx_processor.outcome_revert(outcome_tx)
        raise e


def revert_unfinished(user):
    """Revert failed outcome transactions for given user
    Outcome transactions can fails if internal error happend or server shutdown.
    Call this function check and to remove unfinished payments for given user.
    """
    def revert(es):
        store = account_store.get_store()
        try:
            store.get_transaction(es.get_external_subtype(), es.get_external_id())
            return True
        except error.Error as e:
            if e.get_error_code() == error.TX_NOT_FOUND:
                return False
    tx_processor.revert_hold_transactions(user, {"user2user": revert})


def __log_info(stage, outcome_tx, income_tx):
    log.l().info('{5} send money from {0}:{1} to {2}:{3} with amount {4}'
        .format(outcome_tx.get_user(), outcome_tx.get_id(), income_tx.get_user(), income_tx.get_id(), outcome_tx.get_amount(), stage))


def __log_error(outcome_tx, income_tx, reason):
    log.l().error('Failed send money from {0}:{1} to {2}:{3} with amount {4}'
        .format(outcome_tx.get_user(), outcome_tx.get_id(), income_tx.get_user(), income_tx.get_id(), outcome_tx.get_amount()))
