import unittest
import json
import os
import rules
import error
import balance
import transaction
import external_service

# TODO: add test for wrong amount parameters


class TestRules(unittest.TestCase):
    def test_load_rules_success(self):
        self.create_test_rules(True, 10, True, 20)
        self.assertEqual(rules.load_rules_config('test_rules.json'), None)

    def test_load_rules_nofile(self):
        self.assertException(lambda: rules.load_rules_config('no_rules.json'), error.WRONG_CONFIG)

    def test_load_wrong_json(self):
        with open("test_rules.json", "w") as f:
            f.write("blajblab")
        self.assertException(lambda: rules.load_rules_config('test_rules.json'), error.WRONG_CONFIG)

    def test_no_configuration(self):
        with open('test_rules.json', 'w') as outfile:
            json.dump({'income': {}, 'outcome': {}}, outfile)
        self.assertException(lambda: rules.load_rules_config('test_rules.json'), error.WRONG_CONFIG)

    def test_no_income_amount(self):
        mi = {'min_amount': {'enabled': True, 'params': {'balala': 'balb'}}}
        ma = {'max_amount': {'enabled': True, 'params': {'amount': 10}}}
        with open('test_rules.json', 'w') as outfile:
            json.dump({'income': ma, 'outcome': mi}, outfile)
        self.assertException(lambda: rules.load_rules_config('test_rules.json'), error.WRONG_CONFIG)

    def test_no_outcome_amount(self):
        mi = {'min_amount': {'enabled': True, 'params': {'amount': 10}}}
        ma = {'max_amount': {'enabled': True, 'params': {}}}
        with open('test_rules.json', 'w') as outfile:
            json.dump({'income': ma, 'outcome': mi}, outfile)
        self.assertException(lambda: rules.load_rules_config('test_rules.json'), error.WRONG_CONFIG)

    def test_income_rule_disabled(self):
        self.create_test_rules(False, 10, True, 20)
        rules.load_rules_config('test_rules.json')
        b = balance.Balance("user", 15, 0)
        tx = transaction.IncomeTransaction(transaction.generate_id(), "user", 10)
        self.assertEqual(rules.test_income(tx, b), None)
    
    def test_income_rule_enabled_error(self):
        self.create_test_rules(True, 10, True, 20)
        rules.load_rules_config('test_rules.json')
        b = balance.Balance("user", 15, 0)
        tx = transaction.IncomeTransaction(transaction.generate_id(), "user", 10)
        try:
            rules.test_income(tx, b),
        except error.Error as e:
            self.assertEqual(e.get_error_code(), error.RULE_VIOLATION)
            self.assertEqual(e.get_error_message().get_name(), 'max_amount')
            self.assertEqual(e.get_error_message().get_parameter(), 'amount')
            self.assertEqual(e.get_error_message().get_parameter_value(), 10)

    def test_income_rule_enabled_pass(self):
        self.create_test_rules(True, 10, True, 20)
        rules.load_rules_config('test_rules.json')
        b = balance.Balance("user", 5, 0)
        tx = transaction.IncomeTransaction(transaction.generate_id(), "user", 10)
        self.assertEqual(rules.test_income(tx, b), None)

    def test_outcome_rule_disabled(self):
        self.create_test_rules(True, 10, False, 15)
        rules.load_rules_config('test_rules.json')
        b = balance.Balance("user", 10, 0)
        exs = external_service.ExternalService(transaction.generate_id(), "user2user", "user2")
        tx = transaction.OutcomeTransaction(transaction.generate_id(), "user", 10, exs)
        self.assertEqual(rules.test_outcome(tx, b), None)

    def test_outcome_rule_enabled_error(self):
        self.create_test_rules(True, 10, True, 15)
        rules.load_rules_config('test_rules.json')
        b = balance.Balance("user", 10, 0)
        exs = external_service.ExternalService(transaction.generate_id(), "user2user", "user2")
        tx = transaction.OutcomeTransaction(transaction.generate_id(), "user", 10, exs)
        try: 
            rules.test_outcome(tx, b)
        except error.Error as e:
            self.assertEqual(e.get_error_code(), error.RULE_VIOLATION)
            self.assertEqual(e.get_error_message().get_name(), 'min_amount')
            self.assertEqual(e.get_error_message().get_parameter(), 'amount')
            self.assertEqual(e.get_error_message().get_parameter_value(), 15)

    def test_outcome_rule_enabled_pass(self):
        self.create_test_rules(True, 10, True, 15)
        rules.load_rules_config('test_rules.json')
        b = balance.Balance("user", 20, 0)
        exs = external_service.ExternalService(transaction.generate_id(), "user2user", "user2")
        tx = transaction.OutcomeTransaction(transaction.generate_id(), "user", 10, exs)
        self.assertEqual(rules.test_outcome(tx, b), None)


    def create_test_rules(self, income_enabled, income_amount, outcome_enabled, outcome_amount):
        ma = {'max_amount': {'enabled': income_enabled, 'params': {'amount': income_amount}}}
        mi = {'min_amount': {'enabled': outcome_enabled, 'params': {'amount': outcome_amount}}}
        data = {'income': ma, 'outcome': mi}
        with open('test_rules.json', 'w') as outfile:
            json.dump(data, outfile)

    def delete_test_rules(self):
        try:
            os.remove('test_rules.json')
        except OSError:
            pass

    def assertException(self, f, expect):
        with self.assertRaises(error.Error) as context:
            f()
        assert context.exception.get_error_code() is expect
        

    def setUp(self):
        #print "TestCase:", self._testMethodName
        pass
    def tearDown(self):
        self.delete_test_rules()
        pass
