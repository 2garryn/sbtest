"""Outcome and income transactions processing module
"""

import error
import abc
from transaction import OutcomeTransaction
from transaction import IncomeTransaction
import transaction
import account_store
import rules
import log


def create_outcome_tx(src_tx_id, src_user, amount, es):
    """Create outcome transaction
    Parameters
    ---------
    src_tx_id : str
        Transaction id
    src_user : str
        User who makes a payment
    amount : int, str
    es : external_service.ExternalService
    """
    return OutcomeTransaction(src_tx_id, src_user, amount, es)


def create_income_tx(dst_tx_id, dst_user, amount):
    """Create income transaction
    Parameters
    ---------
    dst_tx_id : str
        Transaction id
    dst_user : str
        User who receives a payment
    amount : int, str
    """
    return IncomeTransaction(dst_tx_id, dst_user, amount)


def revert_hold_transactions(user, completes):
    """Revert unfinished transactions with HOLD_STATUS
    completes is dictionary where key is external service type and
    value is function that accept external_service.ExternalService from outcome transaction structure
    and returns True or False. True - payment completed sucessfully and transaction should be marked as COMPLETE.
    False - payment failed and transaction should be removed
    """
    store = account_store.get_store()
    txs = store.list_transactions(user, status=transaction.HOLD_STATUS)
    for i, tx in enumerate(txs):
        if isinstance(tx, OutcomeTransaction):
            es = tx.get_external_service()
            et = es.get_external_type()
            if et in completes:
                try:
                    if completes[et](es):
                        outcome_complete(tx)
                    else:
                        outcome_revert(tx)
                except error.Error as e:
                    raise e
                except Exception as internal:
                    raise error.Error(error.INTERNAL_ERROR, str(internal))


def outcome_process(tx):
    """Process outcome transaction"""
    log.l().info("Outcome process: {0}".format(tx))
    ts = account_store.get_store().begin_ts()
    user = tx.get_user()
    balance = ts.get_balance(user)
    rules.test_outcome(tx, balance)
    if not balance.charge(tx.get_amount()):
        log.l().warning("Outcome process not enough money: {0}".format(tx))
        raise error.Error(error.NOT_ENOUGH_MONEY, "not enough money")
    ts.set_balance(balance)
    ts.store_transaction(tx)
    ts.commit()
    log.l().info("Outcome process succesfully for tx: {0}".format(tx.get_id()))


def outcome_complete(tx):
    """Process outcome transaction
    Set transaction status to COMPLETE
    """
    log.l().info("Outcome complete: {0}".format(tx))
    ts = account_store.get_store().begin_ts()
    balance = ts.get_balance(tx.get_user())
    balance.charge_commit(tx.get_amount())
    tx.set_complete()
    ts.set_balance(balance)
    ts.store_transaction(tx)
    ts.commit()
    log.l().info("Outcome complete succesfully for tx: {0}".format(tx.get_id()))


def outcome_revert(tx):
    """Revert hold transaction
    Remove hold transaction from db.
    """
    log.l().info("Outcome revert: {0}".format(tx))
    ts = account_store.get_store().begin_ts()
    if tx.get_status() == transaction.COMPLETE_STATUS:
        log.l().warning("Try to revert success tx: {0}".format(tx))
        raise error.Error(error.CAN_REVERT_COMPLETE, "Success transaction can't be reverted")
    balance = ts.get_balance(tx.get_user())
    balance.charge_rollback(tx.get_amount())
    ts.set_balance(balance)
    ts.revert_transaction(tx)
    ts.commit()
    log.l().info("Outcome revert successfully: {0}".format(tx))


def income_complete(tx):
    """Receive payment to user account
    """
    log.l().info("Income complete: {0}".format(tx))
    ts = account_store.get_store().begin_ts()
    user = tx.get_user()
    balance = ts.get_balance(user)
    rules.test_income(tx, balance)
    balance.add(tx.get_amount())
    ts.set_balance(balance)
    ts.store_transaction(tx)
    ts.commit()
    log.l().info("Income complete succesfully for tx: {0}".format(tx.get_id()))
