import decimal
import error


def to_decimal(amount, zeroAllowed=False, negativeAllowed=False):
    if not isinstance(amount, int) \
        and not isinstance(amount, str) \
            and not isinstance(amount, decimal.Decimal):
        raise error.Error(error.INVALID_AMOUNT, "amount can be only integer or string")
    try:
        dec_amount = decimal.Decimal(amount)
        if not zeroAllowed:
            if dec_amount.is_zero():
                raise error.Error(error.ZERO_AMOUNT, "Zero amount")
        if not negativeAllowed:
            if dec_amount.is_signed():
                raise error.Error(error.NEGATIVE_AMOUNT, "Negative amount")
        return dec_amount
    except error.Error as e:
        raise e
    except Exception:
        raise error.Error(error.INVALID_AMOUNT, str(Exception))
