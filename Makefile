.PHONY: doc
doc:
	mkdir -p doc
	pydoc -w balance transaction account_store external_service rules sbapp tx_processor error log account_file_store utils.py
	mv *.html doc/

clean:
	rm -rf doc/
	rm -f *.pyc

test:
	python -m test

coverage:
	coverage run --source . test.py
	coverage report -m