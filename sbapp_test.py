
import json
import account_file_store
import sbapp
import os
import shutil
import error
import utils
import transaction
import external_service
import tx_processor
import time
import unittest

TEST_RULES = 'test_rules.json'
TEST_DATA_DIR = 'test_data'

class TestSbapp(unittest.TestCase):
    def test_init(self):
        self.init_app()

    def test_create_user_success(self):
        self.init_app()
        self.assertEqual(sbapp.create_user("user1"), None)
        self.assertEqual(sbapp.create_user("user2", 0), None)
        self.assertEqual(sbapp.create_user("user3", 23), None)

    def test_create_user_already_exist(self):
        self.init_app()
        self.assertEqual(sbapp.create_user("user1"), None)
        self.assertException(lambda: sbapp.create_user("user1"), error.ACCOUNT_ALREADY_EXIST)

    def test_create_user_wrong_amount(self):
        self.init_app()
        self.assertException(lambda: sbapp.create_user("user1", -1), error.NEGATIVE_AMOUNT)

    def test_get_balance(self):
        self.init_app()
        self.assertEqual(sbapp.create_user("user1", 94), None)
        b = sbapp.get_balance("user1")
        self.assertEqual(b.get_balance_real(), 94)
        self.assertEqual(b.get_balance_on_hold(), 0)
        self.assertEqual(b.get_user(), "user1")

    def test_user_to_user(self):
        self.init_app()
        self.assertEqual(sbapp.create_user("user1", 90), None)
        self.assertEqual(sbapp.create_user("user2", 5), None)
        self.assertEqual(sbapp.send_money_to_user("user1", "user2", 20), None)
        a = sbapp.get_balance("user1")
        self.assertEqual(a.get_balance_real(), 70)
        self.assertEqual(a.get_balance_on_hold(), 0)
        b = sbapp.get_balance("user2")
        self.assertEqual(b.get_balance_real(), 25)

        outcome = sbapp.transactions("user1")[0]
        self.assertEqual(outcome.get_user(), "user1")
        self.assertEqual(outcome.get_amount(), 20)
        self.assertEqual(outcome.get_status(), transaction.COMPLETE_STATUS)
        self.assertEqual(isinstance(outcome.get_ts(), time.struct_time), True)
        exp_exs = outcome.get_external_service()
        self.assertEqual(exp_exs.get_external_type(), "user2user")
        self.assertEqual(exp_exs.get_external_subtype(), "user2")

        income = sbapp.transactions("user2")[0]
        self.assertEqual(income.get_user(), "user2")
        self.assertEqual(income.get_amount(), 20)
        self.assertEqual(income.get_status(), transaction.COMPLETE_STATUS)
        self.assertEqual(isinstance(income.get_ts(), time.struct_time), True)

    def test_user_to_itself(self):
        self.init_app()
        self.assertEqual(sbapp.create_user("user1", 90), None)
        self.assertException(lambda: sbapp.send_money_to_user("user1", "user1", 10), error.SEND_ITSELF)

    def test_user_to_not_found(self):
        self.init_app()
        self.assertEqual(sbapp.create_user("user1", 90), None)
        self.assertException(lambda: sbapp.send_money_to_user("user1", "user2", 10), error.ACCOUNT_NOT_FOUND)

    def test_revert_unfinished(self):
        self.init_app()
        sbapp.create_user("user1", 90)
        sbapp.create_user("user2", 30)
        stid = transaction.generate_id()
        tx = tx_processor.create_outcome_tx(stid, "user1", 10, external_service.ExternalService(transaction.generate_id(), "user2user", "user2"))
        tx_processor.outcome_process(tx)
        self.assertEqual(sbapp.revert_unfinished("user1"), None)
        self.assertEqual(sbapp.transactions("user1"), [])
        self.assertEqual(sbapp.transactions("user2"), [])

    def test_revert_unfinished_income_success(self):
        self.init_app()
        sbapp.create_user("user1", 90)
        sbapp.create_user("user2", 10)
        outcome_id = transaction.generate_id()
        income_id = transaction.generate_id()

        tx_outcome = tx_processor.create_outcome_tx(outcome_id, "user1", 10, external_service.ExternalService(income_id, "user2user", "user2"))
        tx_processor.outcome_process(tx_outcome)

        tx_income = tx_processor.create_income_tx(income_id, "user2", 10)
        tx_processor.income_complete(tx_income)

        self.assertEqual(sbapp.revert_unfinished("user1"), None)

        out_balance = sbapp.get_balance("user1")
        self.assertEqual(out_balance.get_balance_real(), 80)
        self.assertEqual(out_balance.get_balance_on_hold(), 0)

        in_balance = sbapp.get_balance("user2")
        self.assertEqual(in_balance.get_balance_real(), 20)

        received_out_tx = sbapp.transactions("user1")[0]
        self.assertEqual(received_out_tx.get_id(), outcome_id)
        self.assertEqual(received_out_tx.get_status(), transaction.COMPLETE_STATUS)

    def init_app(self):
        acc_store = account_file_store.AccountManager(TEST_DATA_DIR)
        self.create_test_rules(True, 20, False, 10)
        self.assertEqual(sbapp.init(acc_store, TEST_RULES), None)
    
    def assertException(self, f, expect):
        with self.assertRaises(error.Error) as context:
            f()
        assert context.exception.get_error_code() is expect

    def create_test_rules(self, income_enabled, income_amount, outcome_enabled, outcome_amount):
        ma = {'max_amount': {'enabled': income_enabled, 'params': {'amount': income_amount}}}
        mi = {'min_amount': {'enabled': outcome_enabled, 'params': {'amount': outcome_amount}}}
        data = {'income': ma, 'outcome': mi}
        with open('test_rules.json', 'w') as outfile:
            json.dump(data, outfile)

    def delete_test_rules(self):
        os.remove('test_rules.json')

    def setUp(self):
        pass
    def tearDown(self):
        self.delete_test_rules()
        shutil.rmtree(TEST_DATA_DIR)
        pass
