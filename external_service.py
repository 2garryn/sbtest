class ExternalService:
    """Represents external service to send money"""
    def __init__(self, external_id, external_type, subtype):
        """
        Parameters:
        ----------
        external_id : str
            Id of transaction in external service
        external_type : str
            Type of external service
        external_subtype : str
            Subtype of transaction on external service
        """
        self.__external_id = external_id
        self.__external_type = external_type
        self.__external_subtype = subtype

    def get_external_id(self):
        return self.__external_id

    def get_external_type(self):
        return self.__external_type

    def get_external_subtype(self):
        return self.__external_subtype

    def __str__(self):
        return "{0}-{1}-{2}".format(self.__external_type, self.__external_subtype, self.__external_id)
