import abc
__account_store_impl = None


def set_store(store):
    """Set backend store for accounts and transactions

    Parameters:
    ------------
    store : AccountStore

    """
    global __account_store_impl
    __account_store_impl = store


def get_store():
    global __account_store_impl
    return __account_store_impl


class AccountStore:
    """Abstract class for accounts and transactions store
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_balance(self, user):
        """
        Parameters
        ----------
        user : str
        """
        pass

    @abc.abstractmethod
    def set_balance(self, balance):
        """
        Parameters
        ----------
        balance : balance.Balance
        """
        pass

    @abc.abstractmethod
    def store_transaction(self, tx):
        """
        Parameters
        ----------
        tx : transaction.IncomeTransaction, transaction.OutcomeTransaction
        """
        pass

    @abc.abstractmethod
    def get_transaction(self, user, tid):
        """
        Parameters
        ----------
        user : str
        tid : str
        """
        pass

    @abc.abstractmethod
    def revert_transaction(self, tx):
        """
        Parameters
        ----------
        tx : transaction.IncomeTransaction, transaction.OutcomeTransaction
        """
        pass

    @abc.abstractmethod
    def create_account(self, user, balance):
        """
        Parameters
        ----------
        user : str
        balance : balance.Balance
        """
        pass

    @abc.abstractmethod
    def list_transactions(self, user, status):
        """
        Parameters
        ----------
        user : str
        status : transaction.STATUS_HOLD, transaction.STATUS_COMPLETE
        """
        pass

    @abc.abstractmethod
    def begin_ts(self):
        """Create new database transaction
        """
        pass
