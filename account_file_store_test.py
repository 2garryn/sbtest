import unittest # as unittest
import account_file_store
import balance
import decimal
import shutil
import error
import transaction
import external_service
import tx_processor

TEST_DATA_DIR="test_data"

def td(num):
    return decimal.Decimal(num)

class TestAccountFileStore(unittest.TestCase):
    def test_create_account(self):
        self.createAccount("user1", 10, 0)

    def test_create_already_exist(self):
        self.createAccount("user1", 10, 0)
        self.assertException(lambda: self.createAccount("user1", 10, 0), error.ACCOUNT_ALREADY_EXIST)

    def test_get_balance(self):
        b_exp = balance.Balance("user1", td(12), td(0))
        assert self.store.create_account(b_exp) is None
        b = self.store.get_balance("user1")
        self.assertBalance(b_exp, b)

    def test_get_balance_not_found(self):
        self.assertException(lambda: self.store.get_balance("user456"), error.ACCOUNT_NOT_FOUND)

    def test_set_balance(self):
        self.createAccount("user1", 10, 0)
        b_exp = balance.Balance("user1", td(24.67), td(86))
        assert self.store.set_balance(b_exp) is None
        b = self.store.get_balance("user1")
        self.assertBalance(b_exp, b)

    def test_set_balance_not_found(self):
        b = balance.Balance("user1", td(24.67), td(86))
        self.assertException(lambda: self.store.set_balance(b), error.ACCOUNT_NOT_FOUND)
    
    def test_begin_ts(self):
        ts = self.store.begin_ts()
        self.assertEqual(isinstance(ts, account_file_store.AccountManager), True)
        self.assertEqual(ts.commit(), None)

    def test_replace_tx(self):
        self.createAccount("user1", 10, 0)
        tid = transaction.generate_id()
        income_tx = tx_processor.create_income_tx(tid, "user1", 10)
        assert self.store.store_transaction(income_tx) is None
        assert self.store.store_transaction(income_tx) is None
    
    def test_store_and_get_transaction_income(self):
        self.createAccount("user1", 10, 0)
        src_tx_id = transaction.generate_id()
        es_exp = external_service.ExternalService("none", "user2user", src_tx_id)
        outcome_tx = tx_processor.create_outcome_tx(src_tx_id, "user1", td(10), es_exp)
        assert self.store.store_transaction(outcome_tx) is None
        tx = self.store.get_transaction("user1", src_tx_id)
        self.assertOutcome(outcome_tx, tx)


    def test_store_and_get_transaction_outcome(self):
        self.createAccount("user1", 10, 0)
        tid = transaction.generate_id()
        income_tx = tx_processor.create_income_tx(tid, "user1", td(10))
        assert self.store.store_transaction(income_tx) is None
        tx = self.store.get_transaction("user1", tid)
        self.assertIncome(income_tx, tx)
 

    def test_get_transaction_not_found(self):
        self.createAccount("user1", 10, 0)
        self.assertException(lambda: self.store.get_transaction("user1", "notfoundid"), error.TX_NOT_FOUND)

    def test_get_transaction_user_not_found(self):
        self.assertException(lambda: self.store.get_transaction("user123", "notfoundid"), error.ACCOUNT_NOT_FOUND)

    def test_list_transactions_user_not_found(self):
        self.assertException(lambda: self.store.list_transactions("user123"), error.ACCOUNT_NOT_FOUND)

    def test_list_transactions_empty(self):
        self.createAccount("user1", 10, 0)
        self.assertEqual(self.store.list_transactions("user1"), [])

    def test_list_transactions_status(self):
        self.createAccount("user1", 10, 0)
        src_tx_id = transaction.generate_id()
        es_exp = external_service.ExternalService("none", "user2user", src_tx_id)
        outcome_tx = tx_processor.create_outcome_tx(src_tx_id, "user1", 10, es_exp)
        assert self.store.store_transaction(outcome_tx) is None
        l = self.store.list_transactions("user1", transaction.HOLD_STATUS)
        self.assertEqual(l[0].get_id(), src_tx_id)

    def test_list_transactions(self):
        self.createAccount("user1", 10, 0)
        src_tx_id = transaction.generate_id()
        es_exp = external_service.ExternalService("none", "user2user", src_tx_id)
        outcome_tx = tx_processor.create_outcome_tx(src_tx_id, "user1", td(15.6), es_exp)
        assert self.store.store_transaction(outcome_tx) is None

        dst_tx_id = transaction.generate_id()
        income_tx = tx_processor.create_income_tx(dst_tx_id, "user1", td(15.6))
        assert self.store.store_transaction(income_tx) is None

        txs = self.store.list_transactions("user1")

        assert len(txs) is 2
        if isinstance(txs[0], transaction.OutcomeTransaction):
            self.assertOutcome(outcome_tx, txs[0])
            self.assertIncome(income_tx, txs[1])
        else:
            self.assertOutcome(outcome_tx, txs[1])
            self.assertIncome(income_tx, txs[0])

    def test_revert_transaction(self):
        self.createAccount("user1", 10, 0)
        src_tx_id = transaction.generate_id()
        es_exp = external_service.ExternalService("none", "user2user", src_tx_id)
        outcome_tx = tx_processor.create_outcome_tx(src_tx_id, "user1", td(15.6), es_exp)
        assert self.store.store_transaction(outcome_tx) is None
        assert self.store.revert_transaction(outcome_tx) is None
        self.assertEqual(self.store.list_transactions("user1"), [])


    def assertException(self, f, expect):
        with self.assertRaises(error.Error) as context:
            f()
        assert context.exception.get_error_code() is expect
    def createAccount(self, user, real_balance, hold_balance = 0):
        b = balance.Balance(user, td(real_balance), td(hold_balance))
        assert self.store.create_account(b) is None

    def assertBalance(self, b_exp, b_returned):
        self.assertEqual(b_exp.get_user(), b_returned.get_user())
        self.assertEqual(b_exp.get_balance_real(), b_returned.get_balance_real())
        self.assertEqual(b_exp.get_balance_on_hold(), b_returned.get_balance_on_hold())

    def assertIncome(self, tx_exp, tx_received):
        self.assertEqual(tx_exp.get_user(), tx_received.get_user())
        self.assertEqual(tx_exp.get_id(), tx_received.get_id())
        self.assertEqual(tx_exp.get_amount(), tx_received.get_amount())
        self.assertEqual(tx_exp.get_status(), tx_received.get_status())
        self.assertEqual(tx_exp.get_ts(), tx_received.get_ts())

    def assertOutcome(self, tx_exp, tx_received):
        self.assertEqual(tx_exp.get_user(), tx_received.get_user())
        self.assertEqual(tx_exp.get_id(), tx_received.get_id())
        self.assertEqual(tx_exp.get_amount(), tx_received.get_amount())
        self.assertEqual(tx_exp.get_status(), tx_received.get_status())
        self.assertEqual(tx_exp.get_ts(), tx_received.get_ts())

        es_exp = tx_exp.get_external_service()
        es = tx_received.get_external_service()

        self.assertEqual(es_exp.get_external_id(), es.get_external_id())
        self.assertEqual(es_exp.get_external_type(), es.get_external_type())
        self.assertEqual(es_exp.get_external_subtype(), es.get_external_subtype())

    def setUp(self):
        self.store = account_file_store.AccountManager(TEST_DATA_DIR)
        #print "TestCase:", self._testMethodName
    def tearDown(self):
        shutil.rmtree(TEST_DATA_DIR)
