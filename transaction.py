import random
import string
import time
import utils
import calendar

HOLD_STATUS = "HOLD"
COMPLETE_STATUS = "COMPLETE"


class OutcomeTransaction:
    """OutcomeTransaction instance should be created when money should be charged from user balance.
    This type of transaction additionally has two fields:
        a) status is equal Transaction.HOLD_STATUS or Transaction.COMPLETE_STATUS
        b) externa_service - instance of ExternalService to reprensent destination of payment.
    When transaction is created it has status COMPLETE_STATUS
    """
    def __init__(self, tid, user, amount, external_service):
        """
        Parameters
        -----------
        id : str
            Id of transaction
        user : str
            Owner of transaction.
        amount : int,str
            Amount
        external_service : external_service.ExternalService
            destionation of payment
        """
        self.__id = tid
        self.__user = user
        self.__amount = utils.to_decimal(amount)
        self.__status = HOLD_STATUS
        self.__external_service = external_service
        self.__ts = time.gmtime()

    def get_id(self):
        return self.__id

    def get_user(self):
        return self.__user

    def get_amount(self):
        return self.__amount

    def get_status(self):
        return self.__status

    def get_external_service(self):
        return self.__external_service

    def set_complete(self):
        self.__status = COMPLETE_STATUS

    def get_ts(self):
        return self.__ts

    def __str__(self):
        return 'Outcome transaction: Id: {0} User: {1} Amount: {2} Status: {3} ExtService: {4} Ts: {5}' \
            .format(self.__id, self.__user, self.__amount, self.__status, self.__external_service, calendar.timegm(self.__ts))


class IncomeTransaction:
    """Transaction to receive money by user"""
    def __init__(self, tid, user, amount):
        """
        Parameters
        -----------
        id : str
            Id of transaction
        user : str
            Owner of transaction.
        amount : int,str
            Amount
        """
        self.__id = tid
        self.__user = user
        self.__amount = utils.to_decimal(amount)
        self.__ts = time.gmtime()

    def get_id(self):
        return self.__id

    def get_user(self):
        return self.__user

    def get_amount(self):
        return self.__amount

    def get_status(self):
        return COMPLETE_STATUS

    def get_ts(self):
        return self.__ts

    def __str__(self):
        return 'Income transaction: Id: {0} User: {1} Amount: {2} Status: {3} Ts: {4}' \
            .format(self.__id, self.__user, self.__amount, COMPLETE_STATUS, calendar.timegm(self.__ts))


def generate_id():
    """Generate ID string for transaction."""
    chars = string.ascii_uppercase + string.digits
    return ''.join(random.choice(chars) for x in range(32))
