import pickle
import os
import error
import abc
import log
from account_store import AccountStore


class AccountManager(AccountStore):
    def __init__(self, dir_name):
        log.l().info("Starting with directory {0}...".format(dir_name))
        self.__dir_name = dir_name
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
            log.l().info("Directory created")
        else:
            log.l().info("Directory already exist")
        log.l().info("Started successfully")

    def begin_ts(self):
        return self

    def commit(self):
        pass

    def get_balance(self, user):
        f = self.__account_filename_from_user(user)
        try:
            fo = open(f, "r")
            balance = pickle.load(fo)
            fo.close()
            return balance
        except IOError:
            raise error.Error(error.ACCOUNT_NOT_FOUND, user)

    def set_balance(self, balance):
        file = self.__account_filename_from_user(balance.get_user())
        try:
            fileObject = open(file, 'wb+')
            pickle.dump(balance, fileObject)
            fileObject.close()
        except IOError:
            raise error.Error(error.ACCOUNT_NOT_FOUND, balance.get_user())

    def get_transaction(self, user, tid):
        txs = self.__get_tx_list(user)
        for _, tx in enumerate(txs):
            if tx.get_id() == tid:
                return tx
        raise error.Error(error.TX_NOT_FOUND, "")

    def store_transaction(self, tx):
        txs = self.__get_tx_list(tx.get_user())
        replaced = False
        for i, t in enumerate(txs):
            if t.get_id() == tx.get_id():
                txs[i] = tx
                replaced = True
                break
        if not replaced:
            txs.append(tx)
        self.__set_tx_list(tx.get_user(), txs)

    def revert_transaction(self, tx):
        txs = self.__get_tx_list(tx.get_user())
        # TODO: raise exception on COMPLETE transactions.They can't be reverted
        txs = list(filter(lambda t: t.get_id() != tx.get_id(), txs))
        self.__set_tx_list(tx.get_user(), txs)

    def list_transactions(self, user, status=None):
        txs = self.__get_tx_list(user)
        if status:
            return list(filter(lambda t: t.get_status() == status, txs))
        return txs

    def create_account(self, balance):
        log.l().debug("Create account {0}".format(balance))
        user = balance.get_user()
        try:
            os.mkdir(self.__dir_name + "/" + user)
        except OSError as e:
            if e.errno == os.errno.EEXIST:
                log.l().warning("Already exist account {0}".format(balance))
                raise error.Error(error.ACCOUNT_ALREADY_EXIST, user)
        self.set_balance(balance)
        self.__set_tx_list(user, [])
        log.l().debug("Account created {0}".format(balance))

    def __account_filename_from_user(self, user):
        return self.__dir_name + "/" + user + "/account"

    def __tx_list_filename_from_user(self, user):
        return self.__dir_name + "/" + user + "/txs"

    def __set_tx_list(self, user, tx_list):
        self.get_balance(user)
        f = self.__tx_list_filename_from_user(user)
        fileObject = open(f, "wb+")
        pickle.dump(tx_list, fileObject)
        fileObject.close()

    def __get_tx_list(self, user):
        self.get_balance(user)
        f = self.__tx_list_filename_from_user(user)
        fo = open(f, "r")
        txs = pickle.load(fo)
        fo.close()
        if txs:
            return txs
        else:
            return []
