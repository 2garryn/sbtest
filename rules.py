"""
    This module is responsible for initialization rules and testing transactions on accordance to rules
    Rules are read from config file which is passed to load_rules_config.
    Structure of rules config:
    {
        "income": { /// income rules
            "max_amount": { /// rule name
                "enabled": false, /// rule can be disabled or enabled
                "params": {
                    "amount": 20 /// parameter of rule
                }
            }
        },
        "outcome": { /// outcome rules
            "min_amount": {
                "enabled": false,
                "params": {
                    "amount": 10
                }
            }
        }
    }
"""
import json
import error
import log
import decimal

income_rules = {}
outcome_rules = {}


def test_income(tx, balance):
    """
    Parameters
    -----------
    tx : transaction.IncomeTransaction
    balance : balance.Balance

    Raise error.Error expection with RULE_VIOLATION code and Violation error as message
    """
    __testRules(tx, balance, income_rules)


def test_outcome(tx, balance):
    """
    Parameters
    -----------
    tx : transaction.OutcomeTransaction
    balance : balance.Balance

    Raise error.Error expection with RULE_VIOLATION code and Violation error as message
    """
    __testRules(tx, balance, outcome_rules)


def load_rules_config(filename):
    """Loading rules config from file"""
    log.l().info("Loading rules from {0}".format(filename))
    try:
        with open(filename) as f:
            data = json.load(f)
            __load(data)
            f.close()
    except IOError:
        log.l().error("Can not find rules file {0}".format(filename))
        raise error.Error(error.WRONG_CONFIG, "Can not find rules file {0}".format(filename))
    except ValueError:
        log.l().error("Can not parse rules file {0}".format(filename))
        raise error.Error(error.WRONG_CONFIG, "Can not parse rules file {0}".format(filename))
    log.l().info("Rules loaded sucessfully")


def __testRules(tx, balance, rules):
    for k, v in rules.iteritems():
        res = v(tx, balance)
        if not res[0]:
            log.l().warning("Rule violation. Rule: {0} User:({1}) Tx:({2}) Rule {3}".format(k, balance, tx, res[1]))
            raise error.Error(error.RULE_VIOLATION, res[1])


def income_max_amount(params):
    """Rule.
    This rule checks that user who receive money doesn't have more than 'amount' on his balance.
    Config parameter - 'amount' : str, int
    """
    try:
        params['amount']
    except KeyError:
        raise error.Error(error.WRONG_CONFIG, "No max amount for income_max_amount")
    m = decimal.Decimal(params['amount'])

    def test(tx, balance):
        log.l().debug("Test income_max_amount. Tx - {0} User - {1}".format(tx, balance))
        if balance.get_balance_real() < m:
            return (True, None)
        else:
            return (False, ViolationError("max_amount", "amount", m))
    return test


def outcome_min_amount(params):
    """Rule.
    This rule checks that user who make payment has more than 'amount' on his balance.
    Config parameter - 'amount' : str, int
    """
    try:
        params['amount']
    except KeyError:
        raise error.Error(error.WRONG_CONFIG, "No min amount for outcome_min_amount")
    m = decimal.Decimal(params['amount'])

    def test(tx, balance):
        log.l().debug("Test outcome_min_amount. Tx - {0} User - {1}".format(tx, balance))
        if balance.get_balance_real() > m:
            return (True, None)
        else:
            return (False, ViolationError("min_amount", "amount", m))
    return test


"""Mapping rules in config to rules implementation"""
rules_mapping = {
    'income':
        {
            'max_amount': income_max_amount
        },
    'outcome':
        {
            'min_amount': outcome_min_amount
        }
}


def __load(data):
    global outcome_rules
    global income_rules
    outcome_rules = __load_2(data, 'outcome')
    income_rules = __load_2(data, 'income')


def __load_2(data, rules_type):
    log.l().info("Load {0} rules..".format(rules_type))
    frules = data[rules_type]
    mapping = rules_mapping[rules_type]
    ready_rules = {}
    for k, v in mapping.iteritems():
        if k in frules:
            if frules[k]['enabled']:
                params = frules[k]['params']
                ready_rules[k] = v(params)
                log.l().info("Loaded rule: {0}".format(k))
        else:
            log.l().info("Can't find configuration for {0}".format(k))
            raise error.Error(error.WRONG_CONFIG, "Can't find configuration for {0}".format(k))
    log.l().info("Rules {0} loaded".format(rules_type))
    return ready_rules


class ViolationError():
    """Detailed violation"""
    def __init__(self, name, parameter, parameter_value):
        self.__name = name
        self.__parameter = parameter
        self.__parameter_value = parameter_value

    def get_name(self):
        return self.__name

    def get_parameter(self):
        return self.__parameter

    def get_parameter_value(self):
        return self.__parameter_value

    def __str__(self):
        return "rule: {0}, parameter: {1}, parameter value: {2}".format(self.__name, self.__parameter, self.__parameter_value)
