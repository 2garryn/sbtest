import unittest
import transaction
import external_service
import decimal
import time
import error 
class TestBalance(unittest.TestCase):
    def test_create_outcome(self):
        stid = transaction.generate_id()
        dtid = transaction.generate_id()
        exs = external_service.ExternalService(dtid, "user2user", "user2")
        tx = transaction.OutcomeTransaction(stid, "user", 10, exs)
        self.assertEqual(tx.get_id(), stid)
        self.assertEqual(tx.get_user(), "user")
        self.assertEqual(tx.get_amount(), decimal.Decimal(10))
        self.assertEqual(tx.get_status(), transaction.HOLD_STATUS)
        self.assertEqual(isinstance(tx.get_ts(), time.struct_time), True)

        exp_exs = tx.get_external_service()
        self.assertEqual(exp_exs.get_external_id(), dtid)
        self.assertEqual(exp_exs.get_external_type(), "user2user")
        self.assertEqual(exp_exs.get_external_subtype(), "user2")

    def test_create_income(self):
        tid = transaction.generate_id()
        tx = transaction.IncomeTransaction(tid, "user", 10)
        self.assertEqual(tx.get_id(), tid)
        self.assertEqual(tx.get_user(), "user")
        self.assertEqual(tx.get_amount(), decimal.Decimal(10))
        self.assertEqual(tx.get_status(), transaction.COMPLETE_STATUS)
        self.assertEqual(isinstance(tx.get_ts(), time.struct_time), True)

    def test_outcome_complete(self):
        stid = transaction.generate_id()
        tx = transaction.OutcomeTransaction(stid, "user", 10, self.demo_ext_serv())
        self.assertEqual(tx.get_status(), transaction.HOLD_STATUS)
        self.assertEqual(tx.set_complete(), None)
        self.assertEqual(tx.get_status(), transaction.COMPLETE_STATUS)

    def test_outcome_to_string(self):
        stid = transaction.generate_id()
        dtid = transaction.generate_id()
        exs = external_service.ExternalService(dtid, "user2user", "user2")
        tx = transaction.OutcomeTransaction(stid, "user", 10, exs)
        self.assertEqual(isinstance(str(tx), str), True)

    def test_income_to_string(self):
        tx = transaction.IncomeTransaction(transaction.generate_id(), "user", 10)
        self.assertEqual(isinstance(str(tx), str), True)

    def test_wrong_amount(self):
        self.assertException(lambda: transaction.OutcomeTransaction("blaba", "user", 0, self.demo_ext_serv()), error.ZERO_AMOUNT)
        self.assertException(lambda: transaction.OutcomeTransaction("blaba", "user", -1, self.demo_ext_serv()), error.NEGATIVE_AMOUNT)
        self.assertException(lambda: transaction.OutcomeTransaction("blaba", "user", "abc", self.demo_ext_serv()), error.INVALID_AMOUNT)

        self.assertException(lambda: transaction.IncomeTransaction("blaba", "user", 0), error.ZERO_AMOUNT)
        self.assertException(lambda: transaction.IncomeTransaction("blaba", "user", -1), error.NEGATIVE_AMOUNT)
        self.assertException(lambda: transaction.IncomeTransaction("blaba", "user", "abc"), error.INVALID_AMOUNT)

    def assertException(self, f, expect):
        with self.assertRaises(error.Error) as context:
            f()
        assert context.exception.get_error_code() is expect

    def demo_ext_serv(self):
        return external_service.ExternalService(transaction.generate_id(), "user2user", "user2")

    def setUp(self):
        #print "TestCase:", self._testMethodName
        pass
    def tearDown(self):
        pass