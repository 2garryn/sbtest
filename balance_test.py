import unittest
import decimal
import balance 
import error 

class TestBalance(unittest.TestCase):
    def test_create(self):
        b = balance.Balance("user", "12","0")
        self.assertEqual("user", b.get_user())
        self.assertEqual(decimal.Decimal("12"), b.get_balance_real())
        self.assertEqual(decimal.Decimal("0"), b.get_balance_on_hold())

    def test_wrong_create_params(self):
        self.assertException(lambda: balance.Balance("user", -12, 0), error.NEGATIVE_AMOUNT)
        self.assertException(lambda: balance.Balance("user", 0, -1), error.NEGATIVE_AMOUNT)
        self.assertException(lambda: balance.Balance("user", 3, -1), error.NEGATIVE_AMOUNT)
        self.assertException(lambda: balance.Balance("user", "abc", -1), error.INVALID_AMOUNT)
        self.assertException(lambda: balance.Balance("user", 0, "abc"), error.INVALID_AMOUNT)

    def test_wrong_charge_params(self):
        b = balance.Balance("user", "1238","0")
        self.assertException(lambda: b.charge(-1), error.NEGATIVE_AMOUNT)
        self.assertException(lambda: b.charge(0), error.ZERO_AMOUNT)
        self.assertException(lambda: b.charge("abc"), error.INVALID_AMOUNT)

    def test_wrong_charge_commit_params(self):
        b = balance.Balance("user", 40, 0)
        self.assertEqual(b.charge(30), True)
        self.assertException(lambda: b.charge_commit(-1), error.NEGATIVE_AMOUNT)
        self.assertException(lambda: b.charge_commit(0), error.ZERO_AMOUNT)
        self.assertException(lambda: b.charge_commit("abc"), error.INVALID_AMOUNT)
        self.assertException(lambda: b.charge_commit(40), error.INVALID_AMOUNT)

    def test_wrong_charge_rollback_params(self):
        b = balance.Balance("user", 40, 0)
        self.assertEqual(b.charge(30), True)
        self.assertException(lambda: b.charge_rollback(-1), error.NEGATIVE_AMOUNT)
        self.assertException(lambda: b.charge_rollback(0), error.ZERO_AMOUNT)
        self.assertException(lambda: b.charge_rollback("abc"), error.INVALID_AMOUNT)
        self.assertException(lambda: b.charge_rollback(40), error.INVALID_AMOUNT)

    def test_wrong_add_params(self):
        b = balance.Balance("user", 0, 0)
        self.assertException(lambda: b.add(-1), error.NEGATIVE_AMOUNT)
        self.assertException(lambda: b.add(0), error.ZERO_AMOUNT)
        self.assertException(lambda: b.add("abc"), error.INVALID_AMOUNT)
    

    def test_charge_and_commit(self):
        b = balance.Balance("user", "1238","0")
        self.assertEqual(b.charge("30"), True)
        self.assertEqual(b.get_balance_real(), decimal.Decimal(1208))
        self.assertEqual(b.get_balance_on_hold(), decimal.Decimal(30))
        self.assertEqual(b.charge_commit("10"), None)
        self.assertEqual(b.get_balance_real(), decimal.Decimal(1208))
        self.assertEqual(b.get_balance_on_hold(), decimal.Decimal(20))

    def test_charge_and_rollback(self):
        b = balance.Balance("user", "1238","0")
        self.assertEqual(b.charge("30"), True)
        self.assertEqual(b.get_balance_real(), decimal.Decimal(1208))
        self.assertEqual(b.get_balance_on_hold(), decimal.Decimal(30))
        self.assertEqual(b.charge_rollback("10"), None)
        self.assertEqual(b.get_balance_real(), decimal.Decimal(1218))
        self.assertEqual(b.get_balance_on_hold(), decimal.Decimal(20))

    def test_charge_not_enough_money(self):
        b = balance.Balance("user", "12","0")
        self.assertEqual(b.charge("30"), False)

    def test_add(self):
        b = balance.Balance("user", "12","0")
        self.assertEqual(b.add("30"), None)
        self.assertEqual(b.get_balance_real(), decimal.Decimal(42))
        self.assertEqual(b.get_balance_on_hold(), decimal.Decimal(0))

    def assertException(self, f, expect):
        with self.assertRaises(error.Error) as context:
            f()
        assert context.exception.get_error_code() is expect

    def setUp(self):
        #print "TestCase:", self._testMethodName
        pass
    def tearDown(self):
        pass