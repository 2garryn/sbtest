import account_file_store_test
import balance_test
import transaction_test
import rules_test
import tx_processor_test
import unittest
import error_test
import sbapp_test


loader = unittest.TestLoader()
account_file_store_suite  = unittest.TestSuite()
balance_suite = unittest.TestSuite()
transaction_suite = unittest.TestSuite()
rules_suite = unittest.TestSuite()
tx_processor_suite = unittest.TestSuite()
error_suite  = unittest.TestSuite()
sbapp_suite = unittest.TestSuite()


account_file_store_suite.addTests(loader.loadTestsFromModule(account_file_store_test))
balance_suite.addTests(loader.loadTestsFromModule(balance_test))
transaction_suite.addTests(loader.loadTestsFromModule(transaction_test))
rules_suite.addTests(loader.loadTestsFromModule(rules_test))
tx_processor_suite.addTest(loader.loadTestsFromModule(tx_processor_test))
error_suite.addTest(loader.loadTestsFromModule(error_test))
sbapp_suite.addTest(loader.loadTestsFromModule(sbapp_test))



if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=5).run(account_file_store_suite)
    unittest.TextTestRunner(verbosity=5).run(balance_suite)
    unittest.TextTestRunner(verbosity=5).run(transaction_suite)
    unittest.TextTestRunner(verbosity=5).run(rules_suite)
    unittest.TextTestRunner(verbosity=5).run(tx_processor_suite)
    unittest.TextTestRunner(verbosity=5).run(error_suite)
    unittest.TextTestRunner(verbosity=5).run(sbapp_suite)