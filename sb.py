#!/usr/bin/python

from error import Error
import error
import tx_processor
import account_file_store
import account_store
import transaction
import argparse
import sys
import time
from datetime import datetime
import sbapp
import log


def main():
    init()
    parse_agrs()


def init():
    acc_store = account_file_store.AccountManager("data")
    try:
        sbapp.init(acc_store, 'rules_config.json')
    except error.Error as e:
        report_error(e)
        sys.exit(1)


def parse_agrs():
    parser = argparse.ArgumentParser(description='SB test tool. User to user transaction', prog='./main.py', formatter_class=argparse.RawDescriptionHelpFormatter)
    subparsers = parser.add_subparsers(title="Action", help='Description', dest='subparser_name')

    cu = subparsers.add_parser('create-user', help='Create new user')
    cu.add_argument('-u', '--user', type=str, dest='user', help="Username", required=True)
    cu.add_argument('-a', '--start-amount', default="0", type=str, dest='start_amount', help="Start amount", required=False)

    gb = subparsers.add_parser('balance', help='Get user balance')
    gb.add_argument('-u', '--user', type=str, dest='user', help="Username", required=True)

    gt = subparsers.add_parser('transactions', help='Get user transactions')
    gt.add_argument('-u', '--user', type=str, dest='user', help="Username", required=True)

    gt = subparsers.add_parser('revert-unfinished', help='Revert unfinished transactions.')
    gt.add_argument('-u', '--user', type=str, dest='user', help="Username", required=True)

    sm = subparsers.add_parser('send-money', help='Send money to another user')
    sm.add_argument('-src', '--src_user', type=str, dest='src_user', help="User. Who send money", required=True)
    sm.add_argument('-dst', '--dst_user', type=str, dest='dst_user', help="User. Who send receive", required=True)
    sm.add_argument('-a', '--amount', default="0", type=str, dest='amount', help="Amount to be send", required=True)

    command = parser.parse_args()
    execute_action(command.subparser_name, command)


def execute_action(action, command):
    try:
        if action == 'create-user':
            sbapp.create_user(command.user, command.start_amount)
            print("User created successfully")
        elif action == 'balance':
            b = sbapp.get_balance(command.user)
            print("Balance: {0} \nBalance on hold: {1}".format(b.get_balance_real(), b.get_balance_on_hold()))
        elif action == 'transactions':
            action_transactions(command.user)
        elif action == 'send-money':
            sbapp.send_money_to_user(command.src_user, command.dst_user, command.amount)
            print("Money was sent sucessfully")
        elif action == 'revert-unfinished':
            sbapp.revert_unfinished(command.user)
    except error.Error as e:
        log.l().warning("{0}. code {1}".format(e.get_error_message(), e.get_error_code()))
        report_error(e)
    except Exception as e:
        log.l().fatal(e, exc_info=True)
        print("Unknown error {0}".format(e))


def action_transactions(user):
    txs = sbapp.transactions(user)
    if len(txs) == 0:
        print("No transactions found")
    for _, t in enumerate(txs):
        if isinstance(t, transaction.OutcomeTransaction):
            s = """Outcome transaction:
    Ts: {0}
    Id: {1}
    Amount: {2}
    Owner: {3}
    Status: {4}
    Destination: {5}""".format(convert_time(t.get_ts()), t.get_id(), t.get_amount(), t.get_user(), t.get_status(), t.get_external_service())
            print(s)
        else:
            if isinstance(t, transaction.IncomeTransaction):
                s = """Income transaction:
    Ts: {0}
    Id: {1}
    Status: {2}
    Amount: {3}""".format(convert_time(t.get_ts()), t.get_id(), t.get_status(), t.get_amount())
                print(s)


def report_error(e):
    code = e.get_error_code()
    message = e.get_error_message()
    if code == error.INTERNAL_ERROR:
        print("Internal error")
    elif code == error.NOT_ENOUGH_MONEY:
        print("Not enough money")
    elif code == error.RULE_VIOLATION:
        print_rule_violation(message)
    elif code == error.INVALID_AMOUNT or code == error.NEGATIVE_AMOUNT or code == error.ZERO_AMOUNT:
        print("Wrong amount was passed. Valid amount should be integer or decimal and more than zero")
    elif code == error.WRONG_CONFIG or code == error.CONFIG_NOT_FOUND:
        print("Check rules config")
    elif code == error.ACCOUNT_ALREADY_EXIST:
        print("User already exist")
    elif code == error.ACCOUNT_NOT_FOUND:
        print("User not found: " + message)
    elif code == error.SEND_ITSELF:
        print("Can't send money to the same user")
    else:
        print("Something goes wrong. Code: {0} Message {1}".format(code, message))


def print_rule_violation(message):
    if message.get_name() == "max_amount":
        print("User too rich to get money")
    else:
        print("Rule violation - " + str(message))

def convert_time(ts):
    return datetime.fromtimestamp(time.mktime(ts) - time.timezone)


if __name__ == '__main__':
    main()
