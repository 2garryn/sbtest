import logging
import sys

CURRENT_LOGGER = "sbtest"
FORMATTER = logging.Formatter("%(asctime)s [%(levelname)s] %(name)s - %(module)s:%(funcName)s:%(lineno)s - %(message)s")
# hd = logging.StreamHandler(sys.stdout)
hd = logging.FileHandler("sb.log")
logger = logging.getLogger(CURRENT_LOGGER)
hd.setFormatter(FORMATTER)
logger.setLevel(logging.DEBUG)
logger.addHandler(hd)


def l():
    global logger
    return logger
