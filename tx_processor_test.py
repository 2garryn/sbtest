import unittest
import tx_processor
import transaction
import rules
import external_service
import account_store
import balance
import json
import time
import error
import copy

class TestTxProcessor(unittest.TestCase):
    def test_create_income(self):
        stid = transaction.generate_id()
        received_tx = tx_processor.create_outcome_tx(stid, "user1", 10, self.demo_exs(transaction.generate_id()))
        self.assertEqual(received_tx.get_id(), stid)

    def test_create_outcome(self):
        tid = transaction.generate_id()  
        received_tx = tx_processor.create_income_tx(tid, "user", 10)
        self.assertEqual(received_tx.get_id(), tid)

    def test_outcome(self):
        self.mock_store(balance.Balance("user", 25))
        
        stid = transaction.generate_id()
        dtid = transaction.generate_id()
        tx = tx_processor.create_outcome_tx(stid, "user1", 10, self.demo_exs(dtid))
        self.assertEqual(tx_processor.outcome_process(tx), None)

        store = account_store.get_store()
        self.assertEqual(store.is_commited(), True)
        b = store.get_balance("user1")
        self.assertEqual(b.get_balance_real(), 15)
        self.assertEqual(b.get_balance_on_hold(), 10)

        received_tx = store.get_transaction("user", stid)
        self.assertEqual(received_tx.get_id(), stid)
        self.assertEqual(received_tx.get_status(), transaction.HOLD_STATUS)

    def test_outcome_not_enough_money(self):
        self.mock_store(balance.Balance("user", 25))
        tx = tx_processor.create_outcome_tx(transaction.generate_id(), "user1", 100, self.demo_exs("blabla"))
        self.assertException(lambda: tx_processor.outcome_process(tx), error.NOT_ENOUGH_MONEY)

    def test_outcome_complete(self):
        self.mock_store(balance.Balance("user", 15, 10))
        stid = transaction.generate_id()
        tx = tx_processor.create_outcome_tx(stid, "user1", 10, self.demo_exs(transaction.generate_id()))    
        store = account_store.get_store()
        store.store_transaction(tx)

        self.assertEqual(tx_processor.outcome_complete(tx), None)
        self.assertEqual(store.is_commited(), True)
        b = store.get_balance("user1")
        self.assertEqual(b.get_balance_real(), 15)
        self.assertEqual(b.get_balance_on_hold(), 0)

        received_tx = store.get_transaction("user", stid)
        self.assertEqual(received_tx.get_id(), stid)
        self.assertEqual(received_tx.get_status(), transaction.COMPLETE_STATUS)

    def test_outcome_revert(self):
        self.mock_store(balance.Balance("user", 15, 10))
        store = account_store.get_store()
        stid = transaction.generate_id()
        tx = tx_processor.create_outcome_tx(stid, "user1", 10, self.demo_exs(transaction.generate_id()))    
        store.store_transaction(tx)
        self.assertEqual(tx_processor.outcome_revert(tx), None)
        b = store.get_balance("user1")
        self.assertEqual(b.get_balance_real(), 25)
        self.assertEqual(b.get_balance_on_hold(), 0)

        self.assertException(lambda: store.get_transaction("user", stid), error.TX_NOT_FOUND)

    def test_revert_success_tx(self):
        self.mock_store(balance.Balance("user", 15, 10))
        stid = transaction.generate_id()
        tx = tx_processor.create_outcome_tx(stid, "user1", 10, self.demo_exs(transaction.generate_id())) 
        tx.set_complete()
        self.assertException(lambda: tx_processor.outcome_revert(tx), error.CAN_REVERT_COMPLETE)

    def test_revert_hold_transactions_no_dst(self):
        self.mock_store(balance.Balance("user", 15, 10))
        store = account_store.get_store()
        hold_id = transaction.generate_id()
        hold_tx = tx_processor.create_outcome_tx(hold_id, "user1", 10, self.demo_exs(transaction.generate_id()))    
        store.store_transaction(hold_tx)
        self.assertEqual(tx_processor.revert_hold_transactions("user1", {"user2user": lambda tx: False}), None)
        try:
            store.get_transaction("user1", hold_id)
        except error.Error as e:
            self.assertEqual(e.get_error_code(), error.TX_NOT_FOUND)
            b = store.get_balance("user1")
            self.assertEqual(b.get_balance_real(), 25)
            self.assertEqual(b.get_balance_on_hold(), 0)
        else:
            self.fail("Wrong state. tx found")

    def test_revert_hold_transactions_complete_ok_dst(self):
        self.mock_store(balance.Balance("user", 15, 10))
        store = account_store.get_store()
        complete_id = transaction.generate_id()
        complete_tx = tx_processor.create_outcome_tx(complete_id, "user1", 10, self.demo_exs(transaction.generate_id()))    
        store.store_transaction(complete_tx)
        self.assertEqual(tx_processor.revert_hold_transactions("user1", {"user2user": lambda tx: True}), None)
        b = store.get_balance("user1")
        self.assertEqual(b.get_balance_real(), 15)
        self.assertEqual(b.get_balance_on_hold(), 0)

    def test_completes_error(self):
        self.mock_store(balance.Balance("user", 15, 10))
        store = account_store.get_store()
        complete_id = transaction.generate_id()
        complete_tx = tx_processor.create_outcome_tx(complete_id, "user1", 10, self.demo_exs(transaction.generate_id()))    
        store.store_transaction(complete_tx)
        def comp(tx):
            raise Exception("internal error")
        self.assertException(lambda: tx_processor.revert_hold_transactions("user1", {"user2user": comp}), error.INTERNAL_ERROR)

    def test_income_complete(self):
        self.mock_store(balance.Balance("user", 25))
        tid = transaction.generate_id()
        tx = tx_processor.create_income_tx(tid, "user", 10)
        self.assertEqual(tx_processor.income_complete(tx), None)

        store = account_store.get_store()
        self.assertEqual(store.is_commited(), True)
        b = store.get_balance("user1")
        self.assertEqual(b.get_balance_real(), 35)
        self.assertEqual(b.get_balance_on_hold(), 0)

        received_tx = store.get_transaction("user", tid)
        self.assertEqual(received_tx.get_id(), tid)
        self.assertEqual(received_tx.get_status(), transaction.COMPLETE_STATUS)
   

    def set_rules(self):
        ma = {'max_amount': {'enabled': False, 'params': {'amount': 0}}}
        mi = {'min_amount': {'enabled': False, 'params': {'amount': 0}}}
        data = {'income': ma, 'outcome': mi}
        with open('test_rules.json', 'w') as outfile:
            json.dump(data, outfile)
        rules.load_rules_config('test_rules.json')

    def demo_exs(self, e_id):
        return external_service.ExternalService(e_id, "user2user", "user2")

    def mock_store(self, b):
        account_store.set_store(MockStore(b))

    def assertException(self, f, expect):
        with self.assertRaises(error.Error) as context:
            f()
        assert context.exception.get_error_code() is expect

    def setUp(self):
        self.set_rules()
    def tearDown(self):
        pass

class MockStore(account_store.AccountStore):
    def __init__(self, balance):
        self.__balance = balance
        self.__tx = None
        self.__commited = False
    def begin_ts(self):
        return self

    def get_balance(self, _user):
        return copy.deepcopy(self.__balance)

    def set_balance(self, b):
        self.__balance = copy.deepcopy(b)

    def store_transaction(self, tx):
        self.__tx = copy.deepcopy(tx)

    def commit(self):
        self.__commited = True

    def is_commited(self):
        return self.__commited

    def get_transaction(self, user, tid):
        if self.__tx == None:
           raise error.Error(error.TX_NOT_FOUND, "not found tx")
        return copy.deepcopy(self.__tx)
    def revert_transaction(self, tx):
        self.__tx = None
    def create_account(self, user, balance):
        pass
    def list_transactions(self, user, status):
        if self.__tx != None and self.__tx.get_status() == status:
            return [copy.deepcopy(self.__tx)]
        else:
            return []