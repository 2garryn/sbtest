INTERNAL_ERROR = 1
STORE_ERROR = 2
RULE_VIOLATION = 3
NOT_ENOUGH_MONEY = 4
ACCOUNT_ALREADY_EXIST = 5
ACCOUNT_NOT_FOUND = 6
CONFIG_NOT_FOUND = 7
WRONG_CONFIG = 8
TX_NOT_FOUND = 9
SEND_ITSELF = 10
CAN_REVERT_COMPLETE = 11

INVALID_AMOUNT = 12
NEGATIVE_AMOUNT = 13
ZERO_AMOUNT = 14


class Error(Exception):
    def __init__(self, error_code, error_message):
        self.__error_code = error_code
        self.__error_message = error_message

    def get_error_code(self):
        return self.__error_code

    def get_error_message(self):
        return self.__error_message
